const mongoose = require("mongoose");
const Appointment = mongoose.model("Appointment");

const appointment = {
    newAppointment: function (req, res) {
        var newappointment = {
            name: req.body.name,
            email: req.body.email,
            slot_time: req.body.slot_time,
            slot_date: req.body.slot_date,
            title: req.body.title
        };
        const myData = new Appointment(newappointment);
        myData.
            save().then(result => {
                res.json(result);
                console.log(result);
            });
    },
    listAppointment: function (req, res) {
        Appointment.find({}).then(item => {
            res.json(item);

        });
    },
    editAppointment: function (req, res) {
        Appointment.findById({ _id: req.params.id })
            .then(result => res.json({ result }));
    },
    updateAppointment: function (req, res) {
        Appointment.findByIdAndUpdate(req.params.id, req.body).then(response => {
            res.redirect("/");
        });
    },
    deleteAppointment: function (req, res) {
        Appointment.findByIdAndRemove(req.params.id, req.body).then(res => {
            res.redirect("./");
        });
    }

};
module.exports = appointment;
