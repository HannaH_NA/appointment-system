const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: true });

const appointment = require("../controllers/appointment");

router.post("/appointment", appointment.newAppointment);
router.get("/view", urlencodedParser, appointment.listAppointment);
router.get("/edit/:id", urlencodedParser, appointment.editAppointment);
router.put("/appointment/:id", appointment.updateAppointment);
router.delete("/appointment/:id", appointment.deleteAppointment);

module.exports = router;
