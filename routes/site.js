const express = require('express');
const router = express.Router();
const path = require('path');

/**
 * Site frontend pages
 */
router.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/../views/home.html'));
});
router.get('/view', function (req, res) {
    res.sendFile(path.join(__dirname + '/../views/view.html'));
});
router.get('/edit/:_id', function (req, res) {
    res.sendFile(path.join(__dirname + '/../views/edit.html'));
});
module.exports = router;