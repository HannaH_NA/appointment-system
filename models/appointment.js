const mongoose = require("mongoose");
const Schema = mongoose.Schema;
ObjectId = mongoose.Schema.Types.ObjectId;

const appointmentSchema = new Schema({
    id: ObjectId,
    name: { type: String, required: true },
    email: { type: String, required: true },
    slot_date: { type: String, required: true },
    slot_time: { type: String, required: true },
    title: { type: String, required: true },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Date, required: false }
});

mongoose.model("Appointment", appointmentSchema);
